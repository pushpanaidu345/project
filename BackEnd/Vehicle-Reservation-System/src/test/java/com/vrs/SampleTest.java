package com.vrs;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.vrs.model.User;
import com.vrs.service.UserService;

public class SampleTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("executes before class");
	}

	@Before
	public void setUp() throws Exception {
		System.out.println("executes before each test");
	}

	@Test
	public void test() {
		User user = new User(1, "pushpa", "k", "pushpa", 22, "pushpa@gmail.com");
		
		assertEquals(user, user);

	}

	@After
	public void tearDown() throws Exception {
		System.out.println("executes after each test");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("executes After class");
	}

}
