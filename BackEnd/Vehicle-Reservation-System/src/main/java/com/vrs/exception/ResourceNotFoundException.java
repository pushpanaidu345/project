package com.vrs.exception;

public class ResourceNotFoundException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	final String message;

	public ResourceNotFoundException(String message) {
		super(message);
		this.message = message;
	}
	

}
