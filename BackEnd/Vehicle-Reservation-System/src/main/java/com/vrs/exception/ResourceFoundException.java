package com.vrs.exception;

public class ResourceFoundException extends Exception {
	private static final long serialVersionUID = 1L;
	final String message;

	public ResourceFoundException(String message) {
		super(message);
		this.message = message;
	}

}
