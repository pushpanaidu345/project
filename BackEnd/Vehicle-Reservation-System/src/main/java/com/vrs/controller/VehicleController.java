package com.vrs.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vrs.exception.ResourceNotFoundException;
import com.vrs.model.Vehicle;
import com.vrs.service.VehicleService;

@RestController
@RequestMapping("/vrs")
@CrossOrigin(origins="http://localhost:4200")
public class VehicleController {

	private Logger log=LoggerFactory.getLogger(VehicleController.class);
	@Autowired
	VehicleService vehicleService;

	@PostMapping("/vehicle")
	public ResponseEntity<Vehicle> save(@RequestBody Vehicle vehicle) {
		try {
			log.info(vehicle.getVehicleName());
			vehicle = vehicleService.save(vehicle);
			return new ResponseEntity<>(vehicle, HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
		
	}

	@GetMapping("/vehicle")
	public ResponseEntity<List<Vehicle>> getAllVehicles() {
		List<Vehicle> vehicles = vehicleService.findAll();
		ResponseEntity responseEntity = null;
		if (vehicles != null) {
			log.info(vehicles.get(0).getVehicleName());
			responseEntity = new ResponseEntity(vehicles, HttpStatus.OK);
		} else {
			responseEntity = new ResponseEntity(null, HttpStatus.NO_CONTENT);
		}
		return responseEntity;
	}
	@GetMapping("/vehicle/{id}")
	  public Vehicle getVehicleById(@PathVariable("id") int id) throws ResourceNotFoundException {
	    Optional<Vehicle> vehicleData = vehicleService.findById(id);

	    if (vehicleData.isPresent()) {
	      return vehicleData.get();
	    } else {
	      throw new ResourceNotFoundException("Vehicle Not Found with id "+id);
	    }
	  }
	@PutMapping(value = "/vehicle/update")
	  public Vehicle updateVehicle(@RequestBody Vehicle vehicle) {
	    
	         vehicle = vehicleService.save(vehicle);
	    return vehicle;
	  }
	@DeleteMapping("/vehicle/{id}")
	  public ResponseEntity<HttpStatus> deleteVehicle(@PathVariable("id") int id) {
	    try {
	      vehicleService.deleteById(id);
	     
	      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	    } catch (Exception e) {
	      return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
	    }
	  }
	@GetMapping(value = "vehicle/type/{type}")
	  public ResponseEntity<List<Vehicle>> findByType(@PathVariable String type) {
	    try {
	      List<Vehicle> vehicles = vehicleService.findByType(type);
	    
	      if (vehicles.isEmpty()) {
	        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	      }
	      return new ResponseEntity<>(vehicles, HttpStatus.OK);
	    } catch (Exception e) {
	      return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
	    }
	  }

}
