package com.vrs.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vrs.exception.ResourceFoundException;
import com.vrs.exception.ResourceNotFoundException;
import com.vrs.model.User;
import com.vrs.service.UserService;

@RestController
@RequestMapping("registration")
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {
	private Logger log = LoggerFactory.getLogger(UserController.class);
	@Autowired
	private UserService userService;

	@PostMapping("/registeruser")
	public User registerUser(@RequestBody User user) throws ResourceFoundException  {
		String tempEmail = user.getEmailId();
		log.info(tempEmail);
		if (tempEmail != null && !"".equals(tempEmail)) {
			User userobj = userService.fetchUserByEmailId(tempEmail);
			
			if (userobj != null) {
				throw new ResourceFoundException("email Id with " + tempEmail + " already exists");
			}
		}
		User userObj = null;
		userObj = userService.saveUser(user);

		return userObj;
	}

	@PostMapping("/loginuser")
	public User loginUser(@RequestBody User user) throws ResourceNotFoundException  {
		String emailId = user.getEmailId();
		String password = user.getPassword();
		User userObj = null;
		if (emailId != null && password != null) {
			userObj = userService.getUserByEmailIdAndPassword(emailId, password);
			log.info(emailId);
		}
		if (userObj == null) {
			throw new ResourceNotFoundException("Bad Credentials");
		}
		return userObj;
	}

	@GetMapping("/user")
	public ResponseEntity<List<User>> getAllUsers() {
		List<User> users = userService.findAll();
		if (users != null) {
			return new ResponseEntity<>(users, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}

	}

	@PostMapping("/save")
	public ResponseEntity<User> saveUser(@RequestBody User user) {
		user = userService.save(user);
		return new ResponseEntity<>(user, HttpStatus.OK);
	}
}
