package com.vrs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vrs.model.User;
import com.vrs.repository.UserRepository;
@Service
public class UserServiceImpl implements UserService {
@Autowired
private UserRepository userRepository;
	
	@Override
	public User fetchUserByEmailId(String tempEmail) {
		
		return userRepository.findByUserEmailId(tempEmail);
	}

	@Override
	public User saveUser(User user) {
				return userRepository.save(user);
	}

	@Override
	public User getUserByEmailIdAndPassword(String emailId, String password) {
		
		return userRepository.findByEmailIdAndPassword(emailId,password);
	}

	@Override
	public List<User> findAll() {
		
		return userRepository.findAll();
	}

	@Override
	public User save(User user) {
		return userRepository.save(user);
		
	}

}
