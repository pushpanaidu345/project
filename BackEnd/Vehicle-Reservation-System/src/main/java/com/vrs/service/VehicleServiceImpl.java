package com.vrs.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vrs.model.Vehicle;
import com.vrs.repository.VehicleRepository;
@Service
public class VehicleServiceImpl implements VehicleService {

	@Autowired
	private VehicleRepository vehicleRepository;
	@Override
	public Vehicle save(Vehicle vehicle) {
		
		return vehicleRepository.save(vehicle);
	}

	@Override
	public List<Vehicle> findAll() {
		
		return vehicleRepository.findAll();
	}

	@Override
	public Optional<Vehicle> findById(int id) {
		
		return vehicleRepository.findById(id);
	}

	@Override
	public void deleteById(int id) {
		vehicleRepository.deleteById(id);
	}

	@Override
	public List<Vehicle> findByType(String type) {
		
		return vehicleRepository.findByType(type);
	}

}
