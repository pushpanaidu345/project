package com.vrs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.vrs.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	@Query(value=" select * from user where email_id=:id",nativeQuery=true)
	User findByUserEmailId(@Param("id")String tempEmail);

	User findByEmailIdAndPassword(String emailId, String password);

}
