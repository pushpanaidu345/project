package com.vrs.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.vrs.model.Vehicle;

public interface VehicleRepository extends JpaRepository<Vehicle, Integer> {

	List<Vehicle> findByType(String type);

}
